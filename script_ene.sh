#!/bin/sh

process_portal() {
    mvn clean install -nsu -o -Pstaging && 
    cd portal-application && ./debug_mvn_jetty
}

process_mobile() {
    mvn clean install -Dmaven.test.skip=true &&
    java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=5005,suspend=n -jar target/mobile-core.jar 
}

process_log() {
    ssh ${1} 'sudo tail -f /home/zup/logs/portal.log'
}

process_invalid() {
    echo '\n>  Comando não encontrado e/ou diretório inválido. < < < < <'
}

if [[ $1 == 'log' ]]; then {
    if [[ $2 == 'portal' ]]; then {    
        if [[ $3 == 'a' ]]; then { process_log 'ene@10.51.52.93' }
        elif [[ $3 == 'b' ]]; then { process_log 'ene@10.51.52.94' }
        else { process_invalid } 
        fi;
    }
    else { process_invalid } 
    fi;
} fi;

if [[ $1 == 'run' ]]; then {
    if [[ $2 == 'portal' ]]; then { process_portal } 
    elif [[ $2 == 'mobile' ]]; then { process_mobile }
    else { process_invalid }
    fi;
} fi;